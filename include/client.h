#ifndef CLIENT_H
#define CLIENT_H

struct clientStruct {
    char ip[20];
    int port;
};

typedef struct clientStruct Client;

struct noeud_c {
  Client* c;
  struct noeud_c* suivant;
};

typedef struct noeud_c noeud_c;

char* liste_to_str_c(noeud_c* liste);

#endif