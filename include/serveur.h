#ifndef SERVEUR_H
#define SERVEUR_H

struct serveurStruct {
    char domaine[255];
    char ip[20];
    int port;
};

typedef struct serveurStruct Serveur;

struct noeud_s {
    Serveur* s;
    struct noeud_s* suivant;
};

typedef struct noeud_s noeud_s;

Serveur* serveurNouv(char* domaine, char* ip, int port);
char* reprServeur(Serveur* s);
noeud_s* nouveauNoeud_s(Serveur* s, noeud_s* suivant);
void append_s(noeud_s* tete, Serveur* s);
noeud_s* liste_serveurs(char* filename, char* site);
char* liste_to_str(noeud_s* liste);
#endif