#ifndef DIVERS_H
#define DIVERS_H

#define MAXLINE 1024
char* my_itoa(int num, char* str);
void raler(const char* message);
int endsWith(const char* str, const char* suffix);
char* choisirFichier(const char* adresse);
int max(int x, int y);
int indexOf(char needle, char* haystack);
char* getTime();
char* creer_message(int numero_echange, char* reponse_serveur);
#endif