#include "../include/client.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../include/divers.h"
#include "../include/lecture_csv.h"
#include "../include/serveur.h"


int main(int argc, char** argv) {
  noeud_s* liste = liste_serveurs("./../fichiers_serveurs/serveur_racine.csv");
  printf("%s\n", liste_to_str(liste));

  // Chargment de la liste des serveurs racine
  int nbServeurs = len_liste_c(liste);
  int len_site = strlen(argv[1]);

  // Parsing du site en nom machine, domaine, sous-domaine
  int i = 0;

  char* buf = (char*)malloc((len_site + 1) * sizeof(char));
  strcpy(buf, argv[1]);

  char* sep = ".";
  char* p = strtok(buf, sep);
  char* array[3];

  while (p != NULL) {
    array[i++] = p;
    p = strtok(NULL, sep);
  }

  // sous_domaine("zombie.example.fr") -> .fr
  char* sous_domaine = (char*)malloc(10 * sizeof(char));
  strcat(sous_domaine, ".");
  strcat(sous_domaine, array[2]);

  // domaine_sous_domaine("zombie.example.fr") -> example.fr
  int premier_point = indexOf('.', argv[1]);
  char* domaine_sous_domaine = (char*)malloc(50 * sizeof(char));
  strncpy(domaine_sous_domaine, argv[1] + premier_point + 1, strlen(len_site));

  char reponse[MAXLINE];

  int site_trouve = -1;

  while ((site_trouve == -1) && (liste != NULL)) {
    int socket_desc;
    struct sockaddr_in serveur_addr;
    int serveur_struct_length = sizeof(serveur_addr);

    char message_serveur[MAXLINE];
    char* message_client = reprClient(liste->c);

    if ((socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
      raler("socket");
    }

    serveur_addr.sin_family = AF_INET;
    serveur_addr.sin_addr.s_addr = inet_addr(liste->c->ip);
    serveur_addr.sin_port = htons(liste->c->port);

    if (sendto(socket_desc, message_client, sizeof(message_client), 0,
               (struct sockaddr*)&serveur_addr, serveur_struct_length) < 0) {
      printf("Le serveur %s:%d est inaccessible.", liste->c->ip,
             liste->c->port);
      continue;
    }

    if (recvfrom(socket_desc, message_serveur, sizeof(message_serveur), 0,
                 (struct sockaddr*)&serveur_addr, &serveur_struct_length) < 0) {
      printf("Erreur lors de la réception du message du serveur %s:%d",
             liste->c->ip, liste->c->port);
      continue;
    }

    // On vérifie si le site a été trouvé
    if (strlen(message_serveur) > 2) {
      site_trouve = strstr(message_serveur, argv[1]) != NULL ? 1 : -1;
    }

    if (site_trouve == 1) {
      printf("Site trouvé !");
      break;
    }

    else {
    }
  }

  free(buf);
  free(domaine_sous_domaine);
  free(sous_domaine);
return 0;
}