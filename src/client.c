#include "../include/client.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../include/divers.h"
#include "../include/lecture_csv.h"
#include "../include/serveur.h"

Client* clientNouv(char* ip, int port) {
  Client* c = (Client*)malloc(sizeof(Client));

  if (c == NULL) {
    raler("Client nouv");
  }

  strcpy(c->ip, ip);
  c->port = port;
  return c;
}

Client* csv_to_client(char* line) {
  char separateur = '|';
  int i = 0;
  char* p = strtok(line, separateur);
  char* array[2];

  while (p != NULL) {
    array[i++] = p;
    p = strtok(NULL, separateur);
  }

  return clientNouv(array[0], atoi(array[1]));
}

Client* reprClient(Client* c) {
  char* c_str;
  char buf[10];
  if (c_str = (char*)malloc(50 * sizeof(char)) == NULL) {
    raler("malloc reprClient");
  }

  strcpy(c_str, c->ip);
  strcat(c_str, "|");
  my_itoa(c->port, buf);
  strcat(c_str, buf);
  return c_str;
}

noeud_c* nouveauNoeud_c(Client* c, noeud_c* suivant) {
  noeud_c* new_node = (noeud_c*)malloc(sizeof(noeud_c));

  if (new_node == NULL) {
    raler("nouveauNoeud_c");
  }

  new_node->c = c;
  new_node->suivant = suivant;

  return new_node;
}

noeud_c* append_c(noeud_c* tete, Client* c) {
  noeud_c* cursor = tete;
  while (cursor->suivant != NULL) cursor = cursor->suivant;

  noeud_c* new_node = nouveauNoeud_c(c, NULL);
  cursor->suivant = new_node;
  return tete;
}

noeud_c* liste_serveurs_c(char* filename) {
  noeud_c* liste;
  int compteur = 0;
  FILE* file = fopen(filename, "r");

  if (file == NULL) {
    raler("liste_serveurs_c");
  }

  char line[MAXLINE];

  while (fgets(line, MAXLINE, file)) {
    if (line[0] != '#') {
      Client* c = csv_to_client(line);

      if (compteur != 0) {
        append_s(liste, c);
      }

      else {
        liste = nouveaunoeud_c(c, NULL);
        compteur++;
      }

      free(c);
    }
  }

  if (fclose(file) != 0) {
    raler("fclose");
  }

  return liste;
}

void free_liste_serveurs_c(noeud_c* liste) {
  if (liste == NULL) {
    return;
  }
  free_liste_serveurs_c(liste->suivant);
  free(liste->c);
  free(liste);
}

int len_liste_c(noeud_c* liste) {
  int compteur = 0;
  while (liste != NULL) {
    compteur++;
    liste = liste->suivant;
  }
  return compteur;
}

char* liste_to_str_c(noeud_c* liste) {
  char* s = (char*)malloc(1000 * sizeof(char));

  noeud_c* actuel = liste;
  while (actuel != NULL) {
    strcat(s, reprClient(actuel->c));

    if (actuel->suivant != NULL) {
      strcat(s, ", ");
    }

    actuel = actuel->suivant;
  }

  return s;
}

char* recevoir(Serveur* s) {
  int sockfd;
  char* buffer = (char*)malloc(MAXLINE * sizeof(char));
  char* message = "Message";
  struct sockaddr_in servaddr;

  int n, len;
  int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  if (socket < 0) {
    raler("socket");
  }

  memset(&servaddr, 0, sizeof(servaddr));

  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(s->port);
  servaddr.sin_addr.s_addr = inet_addr(s->ip);

  sendto(sockfd, (const char*)message, strlen(message), 0,
         (const struct sockaddr*)&servaddr, sizeof(servaddr));

  // receive server's response
  printf("Message from server: ");
  n = recvfrom(sockfd, (char*)buffer, MAXLINE, 0, (struct sockaddr*)&servaddr,
               &len);
  puts(buffer);
  close(sockfd);
  return buffer;
}

int main(int argc, char** argv) {
  // Chargment de la liste des serveurs racine
  noeud_c* liste = liste_serveurs_c("../fichiers_serveurs/serveur_racine.csv");
  int nbServeurs = len_liste_c(liste);
  int len_site = strlen(argv[1]);

  char reponse[MAXLINE];

  int site_trouve = -1;

  while ((site_trouve == -1) && (liste != NULL)) {
    int socket_desc;
    struct sockaddr_in serveur_addr;
    int serveur_struct_length = sizeof(serveur_addr);

    char message_serveur[MAXLINE];
    char message_client[1024];
    strcpy(message_client, reprClient(liste->c));

    if ((socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
      raler("socket");
    }

    serveur_addr.sin_family = AF_INET;
    serveur_addr.sin_addr.s_addr = inet_addr(liste->c->ip);
    serveur_addr.sin_port = htons(liste->c->port);

    if (sendto(socket_desc, message_client, sizeof(message_client), 0,
               (struct sockaddr*)&serveur_addr, serveur_struct_length) < 0) {
      printf("Le serveur %s:%d est inaccessible.", liste->c->ip,
             liste->c->port);
      continue;
    }

    if (recvfrom(socket_desc, message_serveur, sizeof(message_serveur), 0,
                 (struct sockaddr*)&serveur_addr, &serveur_struct_length) < 0) {
      printf("Erreur lors de la réception du message du serveur %s:%d",
             liste->c->ip, liste->c->port);
      continue;
    }

    //On vérifie si le site a été trouvé
    if (strlen(message_serveur) > 2) {
      site_trouve = strstr(message_serveur, argv[1]) != NULL ? 1 : -1;
    }

    if (site_trouve == 1) {
      printf("Site trouvé !");
      break;
    }

    else {
      //Appel récursif en parsant la réponse du serveur
    }
  }

  return;
}