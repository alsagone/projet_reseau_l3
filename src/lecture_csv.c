#include "../include/lecture_csv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* getChamp(char* line, int numero)
{
    const char* tok;
    for (tok = strtok(line, "|");
         tok && *tok;
         tok = strtok(NULL, "|\n")) {
        if (!--numero) {
            return tok;
        }
    }

    return NULL;
}

/*int main()
{
    FILE* stream = fopen("input", "r");

    char line[MAXLINE];
    while (fgets(line, MAXLINE, stream)) {
        char* tmp = strdup(line);
        printf("Field 3 would be %s\n", getfield(tmp, 3));
        free(tmp);
    }
}*/