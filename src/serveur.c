#include "../include/serveur.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "../include/divers.h"
#include "../include/lecture_csv.h"

struct timeval {
  long tv_sec;
  long tv_usec;
};

Serveur* serveurNouv(char* domaine, char* ip, int port) {
  Serveur* s = (Serveur*)malloc(sizeof(Serveur));
  strcpy(s->domaine, domaine);
  strcpy(s->ip, ip);
  s->port = port;
  return s;
}

/* Renvoie la représentation sous une chaîne de caractère le contenu de la
 * structure Serveur */
char* reprServeur(Serveur* s) {
  char* str = (char*)malloc(300 * (sizeof(char)));
  char* separateur = " | ";
  char buffer[6];
  my_itoa(s->port, buffer);

  strcpy(str, s->domaine);
  strcat(str, separateur);
  strcat(str, s->ip);
  strcat(str, separateur);
  strcat(str, buffer);
  return str;
}

/*Parse une ligne du fichier csv et renvoie une structure Serveur */
Serveur* csv_to_serveur(char* line) {
  char* separateur = "|";
  int i = 0;
  char* p = strtok(line, separateur);
  char* array[3];

  while (p != NULL) {
    array[i++] = p;
    p = strtok(NULL, separateur);
  }

  return serveurNouv(array[0], array[1], atoi(array[2]));
}

/* Renvoie un nouveau noeud pour la liste chainée de structs Serveur */
noeud_s* nouveauNoeud_s(Serveur* s, noeud_s* suivant) {
  noeud_s* new_node = (noeud_s*)malloc(sizeof(noeud_s));
  if (new_node == NULL) {
    raler("nouveauNoeud_s");
  }
  new_node->s = serveurNouv(s->domaine, s->ip, s->port);
  new_node->suivant = suivant;

  return new_node;
}

/* AJoute un nouveau noeud_s dans la liste */
void append_s(noeud_s* tete, Serveur* s) {
  noeud_s* cursor = tete;
  while (cursor->suivant != NULL) {
    cursor = cursor->suivant;
  }

  noeud_s* new_node = nouveauNoeud_s(s, NULL);
  cursor->suivant = new_node;
  return;
}

/* */
noeud_s* liste_serveurs(char* filename, char* site) {
  FILE* file = fopen(filename, "r");

  if (file == NULL) {
    raler("Erreur ouverture du fichier dans noeud_s liste_serveurs(filename)");
  }

  noeud_s* liste = (noeud_s*)malloc(sizeof(noeud_s));

  if (liste == NULL) {
    raler("malloc");
  }

  int compteur = -1;
  char line[MAXLINE];

  while (fgets(line, MAXLINE, file)) {
    if (line[0] != '#') {
      Serveur* s = csv_to_serveur(line);

      if (strstr(s->domaine, site) != NULL) {
        if (compteur >= 0) {
          append_s(liste, s);
        }

        else {
          liste = nouveauNoeud_s(s, NULL);
          compteur++;
        }
      }

      free(s);
    }
  }

  if (fclose(file) != 0) {
    raler("fclose");
  }

  // Cas où aucun serveur ne matchant la recherche n'a été trouvé
  if (compteur == -1) {
    return NULL;
  }

  return liste;
}

void free_liste_serveurs(noeud_s* liste) {
  if (liste == NULL) {
    return;
  }

  free_liste_serveurs(liste->suivant);
  free(liste->s) ;
  free(liste);
}
char* liste_to_str(noeud_s* liste) {
  if (liste == NULL) {
    return NULL;
  }

  char* s = (char*)malloc(1000 * sizeof(char));

  noeud_s* actuel = liste;
  while (actuel != NULL) {
    strcat(s, reprServeur(actuel->s));

    if (actuel->suivant != NULL) {
      strcat(s, ", ");
    }

    actuel = actuel->suivant;
  }

  return s;
}

/* Renvoie la string domaine1, ip1, port1|domaine2, ip2, port2
 * en une liste chaînée de noeud_s */
noeud_s* str_to_liste(char* str) {
  if ((str == NULL) || (strlen(str) == 0)) {
    return NULL;
  }

  noeud_s* liste;
  int compteur = 0;

  char* serveur_str = strtok(str, ",");

  while (serveur_str != NULL) {
    char* element = strtok(serveur_str, "|");
    int i = 0;
    char* array[3];

    while (element != NULL) {
      array[i++] = element;
      element = strtok(NULL, "|");
    }

    Serveur* s = serveurNouv(array[0], array[1], atoi(array[2]));

    if (compteur != 0) {
      append_s(liste, s);
    }

    else {
      liste = s;
      compteur++;
    }

    serveur_str = strtok(NULL, ",");
  }

  return liste;
}

int main(int argc, char** argv) {
  int port = atoi(argv[2]);
  int socket_desc;
  struct sockaddr_in serveur_addr, client_addr;
  char message_serveur[MAXLINE], message_client[MAXLINE];
  int len_client_struct = sizeof(client_addr);

  memset(message_serveur, '\0', sizeof(message_serveur));
  memset(message_client, '\0', sizeof(message_client));

  if ((socket_desc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    raler("socket serveur");
  }

  serveur_addr.sin_family = AF_INET;
  serveur_addr.sin_port = htons(port);
  serveur_addr.sin_addr.s_addr = inet_addr(argv[1]);

  if (bind(socket_desc, (struct sockaddr*)&serveur_addr, sizeof(serveur_addr)) <
      0) {
    raler("bind");
  }

  // Le serveur est prêt à écouter.
  if (recvfrom(socket_desc, message_client, sizeof(message_client), 0,
               (struct sockaddr*)&client_addr, &len_client_struct) < 0) {
    raler("recvfrom serveur");
  }

  // On récupère l'adresse qu'a envoyé le client
  int i = 0;

  char* buf = (char*)malloc((strlen(argv[1]) + 1) * sizeof(char));
  strcpy(buf, argv[1]);

  char* sep = ".";
  char* p = strtok(buf, sep);
  char* array[3];

  while (p != NULL) {
    array[i++] = p;
    p = strtok(NULL, sep);
  }

  // array[0] -> site
  // array[1] -> ip_client
  // array[2] -> port_client

  char* filename = choisirFichier(array[1]);
  noeud_s* liste = liste_serveurs(filename, array[0]);
  strcpy(message_serveur, liste_to_str(liste));
  free_liste_serveurs(liste);
  free(buf);

  if (sendto(socket_desc, message_serveur, strlen(message_serveur), 0,
             (struct sockaddr*)&client_addr, len_client_struct) < 0) {
    raler("sendto serveur");
  }

  close(socket_desc);
}