#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "../include/divers.h"

char* my_itoa(int num, char* str) {
  if (str == NULL) {
    return NULL;
  }
  sprintf(str, "%d", num);
  return str;
}

void raler(const char* message) {
  perror(message);
  exit(EXIT_FAILURE);
}

char* choisirFichier(const char* adresse) {
  char* filename = (char*)malloc(50 * sizeof(char));
  strcpy(filename, "../fichiers_serveurs/");

  if (strcmp("2001:1234::7", adresse) == 0) {
    strcpy(filename, "serveur_example_fr.csv");
  }

  else if (strcmp("2001:1234::10", adresse) == 0) {
    strcpy(filename, "serveur_fr.csv");
  }

  else if (strcmp("193.0.14.129", adresse) == 0) {
    strcpy(filename, "serveur_racine.csv");
  }

  else {
    free(filename);
    return NULL;
  }

  return filename;
}

char* getTime() {
  struct timeval tv;
  char* t = (char*)malloc(30 * sizeof(char));
  gettimeofday(&tv, NULL);
  snprintf(t, "%ld", tv.tv_sec);
  return t;
}

int max(int x, int y) { return (x > y) ? x : y; }

int indexOf(char needle, char* haystack) {
  char* c = strchr(haystack, needle);

  if (c == NULL) {
    raler("indexOf");
  }

  return (int)(c - haystack);
}

//Renvoie la string "numero_echange | horodatage | reponse_serveur"
char* creer_message(int numero_echange, char* reponse_serveur) {
  char* message;
  char* sep = " | ";

  if ((message = (char*)malloc(MAXLINE * sizeof(char))) == NULL) {
    raler("malloc creer_message");
  }

  strcpy(itoa(numero_echange), message);
  strcat(sep, message);
  strcat(message, getTime());
  strcat(sep, message);
  strcat(reponse_serveur, message);

  return message;
}
